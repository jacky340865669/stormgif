const moment = require('moment');
const fs = require('fs');
const util = require('util');

const timePicker = require('./models/timePicker');
const satelliteImage = require('./models/satelliteImage');
const stormInfo = require('./models/stormInfo');
const async = require("async");

function isTimeBetween(start, between, end){
  const minutesSinceDayStart = function(date){
    return date.getUTCHours() * 60 + date.getUTCMinutes();
  }

  let startLapse = minutesSinceDayStart(start);
  let betweenLapse = minutesSinceDayStart(between);
  let endLapse = minutesSinceDayStart(end);

  if(startLapse > endLapse){
    // start > end indicates there is a day leap
    // condition: start <= between <= max OR min <= between <= end
    return ( startLapse <= betweenLapse ) || ( betweenLapse <= endLapse )
  }else{
    // condition: start <= between <= end
    return ( startLapse <= betweenLapse ) && ( betweenLapse <= endLapse )
  }
}

// combine usage of timePicker and satelliteImage to generator image with time
async function fetchImages(onImageFetched){
  let moments = await timePicker.getStormLifeSpan();

  async.eachLimit(moments, 5, async function(m, callback){
    let date = m.toDate();
    console.log(`start fetching image on ${date.toISOString()})`);
    if(!fs.existsSync(`./output/${date.toISOString()}-output.jpg`)){
      try{
        let path = await satelliteImage.fetchOne({ date: date });
        await onImageFetched({
          date: date,
          path: path
        });
      } catch(err){
        console.log('fetchImages ERROR:');
        console.log(err);
      }
    }
  });
}

const getPrevPin = async function(time){
  let utcTime = moment.utc(time);
  let info = await stormInfo.get();
  if(info[utcTime.valueOf()]){
    return info[utcTime.valueOf()];
  }else{
    return getPrevPin(utcTime.subtract(10, 'minutes'));
  }
}

const getNextPin = async function(time){
  let utcTime = moment.utc(time);
  let info = await stormInfo.get();
  if(info[utcTime.valueOf()]){
    return info[utcTime.valueOf()];
  }else{
    return getNextPin(utcTime.add(10, 'minutes'));
  }
}

const getApproxLocation = async function(time){
  let utcTime = moment.utc(time);
  let info = await stormInfo.get();
  if(info[utcTime.valueOf()]){
    return info[utcTime.valueOf()];
  }else{
    // get linear estimation based on prev and next position
    let nextPin = await getNextPin(time);
    let prevPin = await getPrevPin(time);
    let lat = ((utcTime.diff(prevPin.time) * nextPin.lat) + (nextPin.time.diff(utcTime) * prevPin.lat)) / nextPin.time.diff(prevPin.time)
    let lng = ((utcTime.diff(prevPin.time) * nextPin.lng) + (nextPin.time.diff(utcTime) * prevPin.lng)) / nextPin.time.diff(prevPin.time)
    return {lat: lat, lng: lng};
  }
}

const cropImage = async function(date, execProm){
  let location = await getApproxLocation(date);
  const latHalfSpan = 7.9 * 2.0;
  const lngHalfSpan = 14.05 * 2.0;

  let lLat = location.lat - latHalfSpan;
  let uLat = location.lat + latHalfSpan;
  let lLng = location.lng - lngHalfSpan;
  let uLng = location.lng + lngHalfSpan;

  await execProm(`gdalwarp -overwrite -t_srs "+proj=latlong +ellps=WGS84 +pm=140.7" -te ${lLng} ${lLat} ${uLng} ${uLat} -te_srs "EPSG:4326" -wo SOURCE_EXTRA=100 ./tmp/${date.toISOString()}-geo.tif ./tmp/${date.toISOString()}-cropped.tif`);
}

const run = async() => {

  fetchImages(async function onImageFetched(imageData){
    try{
      let date = imageData.date;
      let sourcePath = imageData.path;
      let isInfrared = imageData.isInfrared;
      let filePrefix = date.toISOString();
      let exec = require('child_process').exec;
      let execProm = util.promisify(exec);

      // overlay coastline
      await execProm(`composite ./coastline-5500.png ${sourcePath} ./tmp/${filePrefix}-coastlined.jpg`)

      // add geographic info on image
      await execProm(`gdal_translate -a_srs "+proj=geos +h=35785863 +a=6378137.0 +b=6356752.3 +lon_0=140.7 +no_defs" -a_ullr -5500000 5500000 5500000 -5500000 ./tmp/${filePrefix}-coastlined.jpg ./tmp/${filePrefix}-geo.tif`);

      // crop satellite image by lat lng
      await cropImage(date, execProm);

      // convert to jpg to reduce file size and allow conversion to mp4
      await execProm(`convert ./tmp/${filePrefix}-cropped.tif ./output/${filePrefix}-output.jpg`)

      // remove tmp images as they take up a lot of space
      fs.unlinkSync(sourcePath);
      fs.unlinkSync(`./tmp/${filePrefix}-coastlined.jpg`);
      fs.unlinkSync(`./tmp/${filePrefix}-geo.tif`);
      fs.unlinkSync(`./tmp/${date.toISOString()}-cropped.tif`);
    }
    catch(err){
      console.log(err);
      console.log('contiuing with error....');
    }
  });
}


run();
