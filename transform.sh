# latitude_of_projection_origin = 0.
# perspective_point_height = 35785863.
# semi_minor = 6356752.3
# longitude_of_projection_origin = 140.7
# grid_mapping_name = "geostationary"
# semi_major = 6378137.

gdal_translate -a_srs "+proj=geos +h=35785863 +a=6378137.0 +b=6356752.3 +lon_0=140.7 +no_defs" -a_ullr -5500000 5500000 5500000 0 northern.jpg temp.tif


# lat span: 15.8 (7.9)
# lng span: 28.1 (14.05)

# 9am: 14.0, 141.2

gdalwarp -overwrite -t_srs "+proj=latlong +ellps=WGS84 +pm=140.7" -te 127.15 6.1 155.25 21.9 -te_srs "EPSG:4326" -wo SOURCE_EXTRA=100 temp.tif cropped.tif

mogrify -format jpg tmp/*.tif

cat ./output/*.jpg | ffmpeg -r 15 -f image2pipe -vcodec mjpeg -analyzeduration 100M -probesize 100M -i - -r 15 -vf scale=1920:1080 -vcodec libx264 ./rammb.mp4


composite -gravity center coastline.png   earth.jpg   compose_over.png


convert bluemarble.jpg infrared-latest.png  -composite  coastline-5500.png -composite infrared-composite.jpg
