const rp = require('request-promise');
const moment = require('moment');

const stormInfo = {}

let cachedInfo = null;

// returns a memorized object, key being a unix timestamp with value being an object containing lat lng
stormInfo.get = async function(){
  if(cachedInfo) { return cachedInfo };

  let info = {};

  console.log('Getting latest storm data...');

  let res = JSON.parse(await rp.get('http://www.gdacs.org/datareport/resources/TC/1000498/geojson_1000498_39.geojson'));
  for(let feature of res.features){
    let geoType = feature.geometry.type;
    if(geoType != 'Point'){ continue; }
    if(!feature.properties.trackdate){ continue; }

    let time = moment.utc(feature.properties.trackdate, 'DD/MM/YYYY HH:mm:ss');

    let timeStamp = time.valueOf();
    let lat = parseFloat(feature.properties.latitude);
    let lng = parseFloat(feature.properties.longitude);

    info[timeStamp] = {lat: lat, lng: lng, time: time};
  }

  cachedInfo = info;

  return info;
}

module.exports = stormInfo;