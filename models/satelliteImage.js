const fs = require('fs');
const http = require('http');
const moment = require('moment');

const satelliteImage = {}

// fetch single disk image, returns file path
satelliteImage.fetchOne = async function(customConfig){
  return new Promise(async function(resolve, reject){
    let imageId = customConfig.date ? customConfig.date.toISOString() : 'latest';
    let filePath = `./tmp/${imageId}-full-disk.jpg`;
    let batchId = moment.utc(customConfig.date).format('YYYYMMDDHHmm00');
    var f = fs.createWriteStream(filePath);
    const timeout = 30000;

    var options = {
        host: 'rammb.cira.colostate.edu',
        port: 80,
        path: `/ramsdis/online/images/hi_res/himawari-8/full_disk_ahi_true_color/full_disk_ahi_true_color_${batchId}.jpg`
    }

    let req = http.get(options,function(res){
      res.on('data', function (chunk) {
        f.write(chunk);
      });
      res.on('end',function(){
        f.end();
        resolve(filePath);
      });
    });

    req.on('error', function(err){
      console.log('fetchOne HTTP ERROR:');
      console.log(err);
      f.end();
      reject(err);
    });

    req.on('socket', function (socket) {
      socket.setTimeout(timeout);
      socket.on('timeout', function() {
        console.log('socket timeout, aborting...');
        req.abort();
        reject('timeout');
      });
    });
  });
}

module.exports = satelliteImage;