const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);

const stormInfo = require('./stormInfo');

const timePicker = {};

// return array of moments with 10 min interval (follow satellite interval)
// time range is starts from when CIMSS has location record
timePicker.getStormLifeSpan = async function(){
  let info = await stormInfo.get();
  let stormDates = Object.values(info).map(x => x.time);
  let start = moment.min(stormDates);
  let end = moment.max(stormDates);

  let range = moment.range(start, end);

  return Array.from(range.by('minute', { step: 10 }));
}

module.exports = timePicker;